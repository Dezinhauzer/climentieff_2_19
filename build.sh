#!/bin/bash
killall -15 plus 2> /dev/null
killall -15 mul 2> /dev/null
killall -15 div 2> /dev/null
killall -15 sqrt 2> /dev/null
rm -rf /tmp/plus
rm -rf /tmp/main
g++ main.cpp HelpService.h HelpService.cpp -o main -lpthread -lrt -std=c++14
g++ plus.cpp HelpService.h HelpService.cpp -o plus -lpthread -lrt -std=c++14
g++ mul.cpp HelpService.h HelpService.cpp -o mul -lpthread -lrt -std=c++14
g++ div.cpp HelpService.h HelpService.cpp -o div -lpthread -lrt -std=c++14
g++ sqrt.cpp HelpService.h HelpService.cpp -o sqrt -lpthread -lrt -std=c++14
#./main