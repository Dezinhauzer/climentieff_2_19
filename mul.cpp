#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include "HelpService.h"

int main() {
    pthread_mutex_t *add_mutex = init_mutex(CREATE, (char *) "mul_mutex");
    if (add_mutex == nullptr) {
        std::cout << "Add mutex wasn't initialized\n";
    }
    pthread_mutex_lock(add_mutex);

    int main, operation;
    main = init_socket(Bind, Main_mul, {});
    operation = init_socket(Connect, Operation, {});

    pthread_mutex_t *mainMutex = init_mutex(GET, (char *) "main_mul_mutex");
    if (mainMutex == nullptr) {
        std::cout << "Mutex wasn't initialized\n";
    }

    // Ожидаем подключение Мьютекса с главного процесса
    sleep(6);
    pthread_mutex_unlock(add_mutex);

    pthread_mutex_lock(mainMutex);
    std::cout << "Process mul was started" << std::endl;
    pthread_mutex_unlock(mainMutex);

    while (1) {
        // Ожидаем вызова с главного потока

        pthread_mutex_lock(add_mutex);
        double a, b;
        a = recvFromSocket(main);
        pthread_mutex_unlock(mainMutex);
        pthread_mutex_lock(add_mutex);
        b = recvFromSocket(main);
        double c = a * b;
        std::cout << a << " * " << b << " = " << c << std::endl;

        sendToSocket(operation, c);
        pthread_mutex_unlock(mainMutex);
    }

    return 0;
}
