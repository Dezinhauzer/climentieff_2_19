
#ifndef CLIMENTIEFF_2_19_HELPSERVICE_H
#define CLIMENTIEFF_2_19_HELPSERVICE_H


#include "sys/socket.h"
#include "netinet/in.h"
#include <fcntl.h>
#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <semaphore.h>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>

enum SOCKET_ACTION {
    Bind,
    Connect
};

enum SOCKET_PORT {
    Main_plus = 2468,
    Main_sqrt = 2467,
    Main_mul = 2466,
    Main_div = 2465,
    Operation = 2469
};

enum mutex_action {
    CREATE, GET
};

pthread_mutex_t *init_mutex(mutex_action action, char *mutex_name);

int init_socket(SOCKET_ACTION action, int port, sockaddr_in addr);

double recvFromSocket(int socket);

void sendToSocket(int socket, double value);


#endif //CLIMENTIEFF_2_19_HELPSERVICE_H
