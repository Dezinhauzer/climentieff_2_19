#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <semaphore.h>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "HelpService.h"
#include <signal.h>

/**
 * Вариант 19 ACJL
 *  A – независимые процессы;
 *  C – мьютексы;
 *  J – сокеты;
 *  L – квадратное уравнение:
 *  D=b^2-4ac,
 *  x1=(-b+sqrtPid(D))/2a
 *  x2=(-b-sqrtPid(D))/2a
 */

pid_t plusPid, mulPid, sqrtPid, divisionPid, mainPid;

double calculateFunction(double &a, double &b, pthread_mutex_t *mainMutex, int mainSocket, int operationSocket,
                         pthread_mutex_t *addMutex) {

    sendToSocket(mainSocket, a);
    pthread_mutex_unlock(addMutex);
    pthread_mutex_lock(mainMutex);

    sendToSocket(mainSocket, b);
    pthread_mutex_unlock(addMutex);
    pthread_mutex_lock(mainMutex);

    return recvFromSocket(operationSocket);
}


void startProcess(const std::string &name) {
    std::string way = "./";
    int id = execv(way.append(name).c_str(), {});
    if (id) {
        std::cout << "Process " << way << " was started." << std::endl;
    }
}

int main() {
    std::cout << "Hi. It's program for calculate ax^2+bx+c" << std::endl;
    /*double a = inputVariable('a'),
            b = inputVariable('b'),
            c = inputVariable('c');*/

    double a = 1;
    double b = -70;
    double c = 600;


    pthread_mutex_t *mainPlusMutex = init_mutex(CREATE, (char *) "main_plus_mutex");
    pthread_mutex_t *mainMulMutex = init_mutex(CREATE, (char *) "main_mul_mutex");
    pthread_mutex_t *mainDivMutex = init_mutex(CREATE, (char *) "main_div_mutex");
    pthread_mutex_t *mainSqrtMutex = init_mutex(CREATE, (char *) "main_sqrt_mutex");
    if (mainPlusMutex == nullptr) {
        std::cout << "Mutex wasn't initialized\n";
    }

    int sock_plus = init_socket(Connect, Main_plus, {}),
            sock_mul = init_socket(Connect, Main_mul, {}),
            sock_sqrt = init_socket(Connect, Main_sqrt, {}),
            sock_div = init_socket(Connect, Main_div, {}),
            operation = init_socket(Bind, Operation, {});

    mainPid = fork();
    if (mainPid) {


        // Ожидаем запуска и инициализации всех процессов.
        sleep(2);
        pthread_mutex_t *addMutex = init_mutex(GET, (char *) "add_mutex");
        pthread_mutex_t *mulMutex = init_mutex(GET, (char *) "mul_mutex");
        pthread_mutex_t *divMutex = init_mutex(GET, (char *) "div_mutex");
        pthread_mutex_t *sqrtMutex = init_mutex(GET, (char *) "sqrt_mutex");

        // ожидаем запуска процесса
        pthread_mutex_lock(addMutex);
        pthread_mutex_lock(mainPlusMutex);
        std::cout << "Plus init" << std::endl;
        pthread_mutex_lock(mulMutex);
        pthread_mutex_lock(mainMulMutex);
        std::cout << "Mul init" << std::endl;
        pthread_mutex_lock(divMutex);
        pthread_mutex_lock(mainDivMutex);
        std::cout << "Div init" << std::endl;
        pthread_mutex_lock(sqrtMutex);
        pthread_mutex_lock(mainSqrtMutex);
        std::cout << "Sqrt init" << std::endl;

        double powB2 = calculateFunction(b, b, mainMulMutex, sock_mul, operation, mulMutex);
        double four = 4;
        double a4 = calculateFunction(a, four, mainMulMutex, sock_mul, operation, mulMutex);
        double ac4 = calculateFunction(a4, c, mainMulMutex, sock_mul, operation, mulMutex);
        ac4 = -ac4;
        double diskriminant = calculateFunction(powB2, ac4, mainPlusMutex, sock_plus, operation, addMutex);
        double sqrtDiskriminant = calculateFunction(diskriminant, ac4, mainSqrtMutex, sock_sqrt, operation, sqrtMutex);
        double minusB = -b;
        double two = 2;
        double a2 = calculateFunction(a, two, mainMulMutex, sock_mul, operation, mulMutex);
        double x1 = calculateFunction(minusB, sqrtDiskriminant, mainPlusMutex, sock_plus, operation, addMutex);
        x1 = calculateFunction(x1, a2, mainDivMutex, sock_div, operation, divMutex);

        double minusSqrtDiskriminant = -sqrtDiskriminant;
        double x2 = calculateFunction(minusB, minusSqrtDiskriminant, mainPlusMutex, sock_plus, operation, addMutex);
        x2 = calculateFunction(x2, a2, mainDivMutex, sock_div, operation, divMutex);


        std::cout << "D = " << diskriminant << std::endl;
        std::cout << "sqrt( D ) = " << sqrtDiskriminant << std::endl;
        std::cout << "x1 = " << x1 << std::endl;
        std::cout << "x2 = " << x2 << std::endl;

        kill(plusPid, SIGKILL);
        kill(sqrtPid, SIGKILL);
        kill(mulPid, SIGKILL);
        kill(divisionPid, SIGKILL);
        return 0;
    }

    plusPid = fork();
    if (plusPid) {
        startProcess("plus");

        return 0;
    }


    mulPid = fork();
    if (mulPid) {
        startProcess("mul");

        return 0;
    }

    divisionPid = fork();
    if (divisionPid) {
        startProcess("div");

        return 0;
    }

    sqrtPid = fork();
    if (sqrtPid) {
        startProcess("sqrt");

        return 0;
    }

    return 0;
}

