#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include "HelpService.h"
#include "netinet/in.h"
#include "sys/socket.h"


pthread_mutex_t *init_mutex(mutex_action action, char *mutex_name) {
    int shm_fd;
    if (action == CREATE) {
        shm_fd = shm_open(mutex_name, O_RDWR | O_CREAT, 0777);
    }

    if (action == GET) {
        shm_fd = shm_open(mutex_name, O_RDWR, 0777);
    }

    if (shm_fd == -1) {
        std::cout << mutex_name << "\tShared memory descriptor wasn't initialized\n";
        return {};
    }

    if (ftruncate(shm_fd, sizeof(pthread_mutex_t)) != 0) {
        std::cout << "File truncate was failed\n";
        return {};
    }

    void *addr = mmap(
            nullptr,
            sizeof(pthread_mutex_t),
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            shm_fd,
            0
    );
    if (addr == MAP_FAILED) {
        std::cout << "Shared address wasn't initialized\n";
        return {};
    }

    auto *mutex = (pthread_mutex_t *) addr;
    if (action == CREATE) {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
        pthread_mutex_init(mutex, &attr);
    }

    return mutex;
}

int init_socket(SOCKET_ACTION action, int port, sockaddr_in addr) {
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(action == Bind ? INADDR_ANY : INADDR_LOOPBACK);

    if (action == Bind && bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        return -1;
    }

    if (action == Connect && connect(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        return -1;
    }

    return sock;
}

double recvFromSocket(int socket) {
    double value;
    recvfrom(socket, reinterpret_cast<void *>(&value), sizeof(value), 0, NULL, NULL);
    return value;
}

void sendToSocket(int socket, double value) {
    send(socket, &value, sizeof(value), 0);
}